## Introduction

This tool takes a collection of biotools entries and compute quality metrics and scores as defiened by the OpenEBench team [(link)](https://docs.google.com/spreadsheets/u/1/d/1H-V3rGhp9P6QQ65BvZPCv8iW73Cd3mJjjsiwz7JxtUw/edit?usp=drive_web&ouid=107587189780281558379]).

## Usage

```
python3 biotools_quality_assessment.py config.yaml
```

### Config file
yaml file that specifies the input and output files paths
```
input_file: /home/eva/FAIRplus/tooling/tools_discovery/output/run_3.json
output_path: /home/eva/FAIRplus/tooling/tool_quality_assessment/output
output_name: run_3
```

## Requirements
The packages:
- `pyYAML` 
- `pandas` 
are needed to run this program. 
A virtual environment can be used. To do so, run:

```
pipenv install
```
Then, do not forget to run the program in that environment either from `pipenv shell` or using `pipenv run` before each command.

Alternaively to using a virtual environment, you can install the packages in your system:
```
python3 -m pip install pyYAML
```
```
python3 -m pip install pandas
```